from django.shortcuts import render
from home.models import *

# Create your views here.
def home(req):
    about = About.objects.order_by('-id')[:1]
    service = Service.objects.order_by('-id')[:1]
    serviceItem = ServiceItem.objects.order_by('-id')
    vision = Vision.objects.order_by('-id')
    visionItem = VisionItem.objects.order_by('-id')
    coreValue = CoreValue.objects.order_by('-id')
    application = Application.objects.order_by('-id')
    partner = Partner.objects.order_by('-id')
    ourteam = Ourteam.objects.order_by('-id')
    contactus = ContactUs.objects.order_by('-id')
    award = Award.objects.order_by('id')
    
    return render(req,'home/home.html',{
        'about':about,
        'service':service,
        'serviceItem':serviceItem,
        'vision':vision,
        'visionItem':visionItem,
        'coreValue':coreValue,
        'application':application,
        'partner':partner,
        'ourteam':ourteam,
        'contactus':contactus,
        'award':award,
    })


