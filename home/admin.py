from django.contrib import admin
from home.models import *
from django.contrib.admin import TabularInline, StackedInline, site
from super_inlines.admin import SuperInlineModelAdmin, SuperModelAdmin


admin.site.register(About)
admin.site.register(Service)
admin.site.register(ServiceItem)
admin.site.register(Vision)
admin.site.register(VisionItem)
admin.site.register(CoreValue)
admin.site.register(Application)
admin.site.register(Partner)
admin.site.register(Ourteam)
admin.site.register(ContactUs)
admin.site.register(Award)