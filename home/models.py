# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

class About(models.Model):
    title = models.TextField(null=True)
    text = models.TextField(null=True)
    
    class Meta:
        verbose_name_plural = "about 청포도"

class Service(models.Model):
    title = models.TextField(null=True)
    
    class Meta:
        verbose_name_plural = "청포도 주요사업"
    
class ServiceItem(models.Model):
    title = models.TextField(null=True)
    text = models.TextField(null=True)

    class Meta:
        verbose_name_plural = "청포도 주요사업 내용"
        
        
class Vision(models.Model):
    title = models.TextField(null=True)
    
    class Meta:
        verbose_name_plural = "vision"
    
class VisionItem(models.Model):
    title = models.TextField(null=True)
    text = models.TextField(null=True)
    
    class Meta:
        verbose_name_plural = "vision"
    
    
class CoreValue(models.Model):
    title = models.TextField(null=True)
    text = models.TextField(null=True)

    class Meta:
        verbose_name_plural = "core value"
    
    
class Application(models.Model):
    title = models.TextField(null=True)
    
    
    class Meta:
        verbose_name_plural = "청포도 application"
    
    

class Partner(models.Model):
    title = models.TextField(null=True)

    
    class Meta:
        verbose_name_plural = "청포도 partners"
    

class Ourteam(models.Model):
    title = models.TextField(null=True)
    
    
class ContactUs(models.Model):
    name = models.CharField(max_length=20, null=True, verbose_name="대표")
    number = models.CharField(max_length=40,null=True, verbose_name="사업자번호")
    dom = models.CharField(max_length=40,null=True, verbose_name="주소")
    tel = models.CharField(max_length=20,null=True, verbose_name="전화번호")
    
    class Meta:
        verbose_name_plural = "contact us"
        
        

class Award(models.Model):
    name = models.CharField(max_length=50,null=True, verbose_name="수상실적")
    
    class Meta:
        verbose_name_plural = "수상실적"