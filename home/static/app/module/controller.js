(function(){
    var app = angular.module('juice.module.controller', [
        'juice.controller.home.header',
        'juice.controller.home.contact',
        'juice.controller.home.login',
        'juice.controller.dashboard.dashboard',
    ]);
})();