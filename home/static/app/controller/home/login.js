(function(){
    var app = angular.module('juice.controller.home.login', []);
    
    app.controller('loginCtrl', function($scope){
        $('input').keypress(function(event){
            if (event.which == 13) {
                $('.btn').click();
            }
        });
        $('.btn').click(function(){
            $.ajax({
               type:'post',
               url:'/login',
               data:{
                   'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val(),
                   'username':$('.username').val(),
                   'password':$('.password').val(),
               }, success:function(result){
                   if (result == 'success') {
                       location.href="/dashboard/";
                   } else {
                        $('.alert-warning').fadeIn();
                   }
               }, error:function(result) {
                        $('.alert-danger').fadeIn();
                   
               }
            });
        });
    });
})();