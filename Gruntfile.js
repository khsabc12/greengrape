module.exports = function(grunt) {
    grunt.initConfig({
        
        uglify: {
            options:{
                mangle: false
            },
            my_target: {
                files: {
                    'home/static/build/js/custom.min.js':[
                        'home/static/app/**/**/*.js',
                    ]
                }
            }
        },
        less: {
            production: {
                files: {
                    'home/static/build/css/custom.min.css':[
                        'home/static/app/less/index.less'
                    ]
                }
            }
        },
        
    });
    
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    
    
    grunt.registerTask('default',[
        'uglify',
        'less',
        ]);
};